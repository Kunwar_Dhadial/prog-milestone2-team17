﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team17_task3
{
    class Program
    {
        static Dictionary<string, string> vegList;
        static void Main(string[] args)
        {
            team17();
        }
        static void team17()
        {
            Console.Clear();
            Console.WriteLine("######################################");
            Console.WriteLine("#  ________________________________  #");
            Console.WriteLine("# |                                | #");
            Console.WriteLine("# |         Team 17 Menu           | #");
            Console.WriteLine("# |________________________________| #");
            Console.WriteLine("#                                    #");
            Console.WriteLine("#                                    #");
            Console.WriteLine("#   1. Date Calculator          (1)  #");
            Console.WriteLine("#   2. Grade Average Calculator (2)  #");
            Console.WriteLine("#   3. Random Number Calculator (3)  #");
            Console.WriteLine("#   4. Favorite Food Rating     (4)  #");
            Console.WriteLine("#                                    #");
            Console.WriteLine("#   Please select an option :        #");
            Console.WriteLine("######################################");
            var menu = int.Parse(Console.ReadLine());

            if (menu == 1)
            {
                Console.Clear();
                Console.WriteLine("*       Date Calculator Menu         *");
                Console.WriteLine("                                     ");
                Console.WriteLine("     1. Days old Calculator      (1) ");
                Console.WriteLine("     2. Days in Years Calculator (2) ");
                Console.WriteLine("                                     ");
                Console.WriteLine("      Please select an option :       ");
                var selection = int.Parse(Console.ReadLine());

                if (selection == 1)
                {
                    Console.Clear();
                    Console.WriteLine("");
                    Console.WriteLine("  Press m to return to the main menu");
                }
                if (selection == 2)
                {
                    Console.Clear();
                    Console.WriteLine("");
                }
            }
            if (menu == 2)
            {
                Console.Clear();
                Console.WriteLine("Welcome to the Grade Average Calculator");
                addInfo();
            }

            if (menu == 3)
            {
                Console.Clear();
                randGame();
            }
            if (menu == 4)
            {
                Console.Clear();
                addFood();
            }
        }

        static void addInfo()
        {
            var student = new List<Tuple<string, string, string, int, string, int, int>> { };
            Console.WriteLine("Are you level 5 or level 6?");
            int level = int.Parse(Console.ReadLine());
            int count = 0;
            if (level == 5) { count = 4; }
            if (level == 6) { count = 3; }
            Console.WriteLine($"You have selected level {level}. Please enter the names of the level {level} papers you are taking");
            var i = 0;
            var courses = new List<string> { };
            while (i < count)
            {
                Console.WriteLine($"Please enter paper #{i + 1}/{count}");
                courses.Add(Console.ReadLine());
                i++;
            }
            Console.WriteLine("Please enter your student ID number");
            var studentId = Console.ReadLine();
            var x = 0;
            var studentInfo = new List<Tuple<string, string, string, int, string, int, int>> { };
            while (x < count)
            {
                Console.WriteLine($"Please enter the paper code for {courses[x]}");
                var paperCode = Console.ReadLine();
                Console.WriteLine($"Please enter your percentage mark for {courses[x]}");
                var percentage = int.Parse(Console.ReadLine());
                var grade = gradeLetter(percentage);
                studentInfo.Add(Tuple.Create(courses[x], paperCode, grade, percentage, studentId, count, level));
                x++;
            }
            Console.Clear();
            Console.WriteLine("");
            Console.WriteLine("*** STUDENT INFORMATION ADDED***");
            Info(studentInfo);
            }
        static void Info(List<Tuple<string, string, string, int, string, int, int>> student)
        {
            Console.WriteLine("");
            Console.WriteLine("Please select an option");
            Console.WriteLine("1. Re-Enter student information");
            Console.WriteLine("2. View student and class information");
            Console.WriteLine("3. View overall course mark");
            Console.WriteLine("4. View papers where A+ was achieved");
            Console.WriteLine("5. Return to main menu");
            var input = int.Parse(Console.ReadLine());
            switch (input)
            {
                case 1:
                    Console.Clear();
                    addInfo();
                    break;
                case 2:
                    Console.Clear();
                    studentMarks(student);
                    break;
                case 3:
                    Console.Clear();
                    averageGrade(student);
                    break;
                case 4:
                    Console.Clear();
                    highGrade(student);
                    break;
                case 5:
                    team17();
                    break;
                default:
                    student.Add(Tuple.Create("", "", "", 0, "", 0, 0));
                    break;
            }
        }
        public static void studentMarks(List<Tuple<string, string, string, int, string, int, int>> student)
        {
            var i = 0;
            Console.WriteLine($"Student ID: {student[0].Item5}. Studying: level {student[0].Item7}");
            Console.WriteLine("Papers taken and grades:");
            while (i < student[0].Item6)
            {
                Console.WriteLine($"Paper: {student[i].Item2} - {student[i].Item1}. Percentage: {student[i].Item4}. Grade: {student[i].Item3}");
                i++;
            }
            Info(student);
        }
        public static string gradeLetter(int percentage)
        {
            if (percentage >= 90) { var grade = "A+"; return grade; }
            if (percentage >= 85 && percentage <= 89) { var grade = "A"; return grade; }
            if (percentage >= 80 && percentage <= 84) { var grade = "A-"; return grade; }
            if (percentage >= 75 && percentage <= 79) { var grade = "B+"; return grade; }
            if (percentage >= 70 && percentage <= 74) { var grade = "B"; return grade; }
            if (percentage >= 65 && percentage <= 69) { var grade = "B-"; return grade; }
            if (percentage >= 60 && percentage <= 64) { var grade = "C+"; return grade; }
            if (percentage >= 55 && percentage <= 59) { var grade = "C"; return grade; }
            if (percentage >= 50 && percentage <= 54) { var grade = "C-"; return grade; }
            if (percentage >= 40 && percentage <= 49) { var grade = "D"; return grade; } else { var grade = "E"; return grade; }
        }
        public static void averageGrade(List<Tuple<string, string, string, int, string, int, int>> student)
        {
            int count = 0;
            if (student[0].Item7 == 5) { count = 4; }
            if (student[0].Item7 == 6) { count = 3; }
            var i = 0;
            var gradeTotal = 0;
            while (i < count)
            {
                gradeTotal = student[i].Item4 + gradeTotal;
                i++;
            }
            var gradetotal = gradeTotal / count;
            var overallGrade = gradeLetter(gradetotal);
            Console.WriteLine($"Overall Percentage: {gradetotal} Grade: {overallGrade} ");
            Info(student);
        }
        public static void highGrade(List<Tuple<string, string, string, int, string, int, int>> student)
        {
            int count = 0;
            if (student[0].Item7 == 5) { count = 4; }
            if (student[0].Item7 == 6) { count = 3; }
            var i = 0;
            Console.WriteLine("Papers where A+ was achieved(90%+)");
            while (i < count)
            {
                if (student[i].Item4 >= 90)
                {
                    Console.WriteLine($"Paper: {student[i].Item2} - {student[i].Item1}. Percentage: {student[i].Item4}. Grade: {student[i].Item3}");
                }
                i++;
            }
            Info(student);
        }
        static void randGame()
        {

            int roundcounter = 0;

            int score = 0;

            int totalgamesplayed = 0;

            int round = 0;

            bool repeat = true;

            while (repeat) //**** While loop to repeat game once finished ****//
            {

                //**** Starting message and totalgamesplayed +1 at the begining of game code ****//
                totalgamesplayed++;

                Console.WriteLine("Hello!");
                Console.WriteLine();
                Console.WriteLine("In this game you have to guess what number is going to appear 1-5.");
                Console.WriteLine();
                Console.WriteLine("Good luck!");
                Console.WriteLine();
                Console.WriteLine("Press any key to begin");
                Console.WriteLine();
                Console.ReadKey();
                Console.Clear();


                do
                {
                    round++;

                    Console.WriteLine($"Round {round}. Guess a number between 1 and 5.");
                    Console.WriteLine();

                    Random rnd = new Random();
                    int ranvalue = rnd.Next(1, 5); //**** Random value between 1 and 5 ****//

                    int userguess = Int32.Parse(Console.ReadLine()); //**** User guess added to int userguess ****//

                    Console.WriteLine();
                    Console.WriteLine($"The number to guess is {ranvalue}!"); //**** random number set for userguess int to be compared to ****//



                    if (userguess == ranvalue) //**** if statement to compare userguess value to ranvalue value ****//
                    {
                        score++; //**** If correct adds 1 point to user score via int score ****//

                        Console.WriteLine();
                        Console.WriteLine($"You guessed correctly! your score is now {score}");
                        Console.WriteLine();

                        roundcounter++;
                    }

                    else //**** Otherwise provides an incorrect message to user ****//
                    {
                        Console.WriteLine();
                        Console.WriteLine("Incorrect!");
                        Console.WriteLine();

                        roundcounter++;
                    }

                } while (roundcounter < 5); //**** Max number of rounds set to 5 ****//
                {
                    //**** Asks user if they want to play again or exit ****//
                    Console.Clear();
                    Console.WriteLine();
                    Console.WriteLine($"Thanks for playing! Your final score was {score}.");
                    Console.WriteLine();
                    Console.WriteLine("Play again... ?");
                    Console.WriteLine();
                    Console.WriteLine("Y / N");
                    Console.WriteLine();

                    var again = Console.ReadLine();

                    //**** If statement dependant on user input ****//
                    if (again == "y")
                    {
                        score = 0;
                        roundcounter = 0; //**** Resets previous game values for next game ****//
                        round = 0;

                        Console.Clear();
                    }

                    else
                    {
                        //**** Exit message, score and total games user has played displayed****//

                        Console.Clear();
                        Console.WriteLine($"Your final score was {score}, and you've played a total of {totalgamesplayed} games!");
                        Console.WriteLine();
                        Console.WriteLine("Thanks for playing! Press enter to exit.");
                        Console.WriteLine();
                        Environment.Exit(0);
                    }
                }

            }

        }
        static void addFood()
        {
            int ch;
            do
            {
                Console.WriteLine("1. Add Foods");
                Console.WriteLine("2. Show Foods");
                Console.WriteLine("3. Change Rate of Food");
                Console.WriteLine("4. Exit");
                Console.WriteLine("Enter your Choice");
                ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        addfoods();
                        break;
                    case 2:
                        showfood();
                        break;
                    case 3:
                        changerate();
                        break;
                    case 4:
                        Console.WriteLine("Bye !!!!!");
                        break;
                }


            } while (ch != 4);
        }
        public static void addfoods()
        {
            int i;

            vegList = new Dictionary<string, string>();
            for (i = 1; i <= 5; i++)
            {
                Console.WriteLine("Enter Food " + i);
                string food = Console.ReadLine();
                Console.WriteLine("Rate for Food " + i + " (1-5)");

                string rate = Console.ReadLine();
                int rate_value;


                if (int.TryParse(rate, out rate_value))
                {
                    vegList.Add(food, rate);


                }
                else
                {
                    Console.WriteLine("Rate Should be an integer!");
                    i--;

                }


            }

        }
        public static void showfood()
        {
            int v1, j;
            for (j = 1; j <= 5; j++)
            {
                foreach (KeyValuePair<string, string> veg1 in vegList)
                {
                    if (int.TryParse(veg1.Value, out v1))
                    {

                        if (v1 == j)
                        {
                            Console.WriteLine("Food = {0}, Rate = {1}", veg1.Key, veg1.Value);
                        }
                    }
                }


            }

        }
        public static void changerate()
        {
            int v1, j;
            foreach (KeyValuePair<string, string> veg1 in vegList)
            {
                if (int.TryParse(veg1.Value, out v1))
                {


                    Console.WriteLine("Food = {0}, Rate = {1}", veg1.Key, veg1.Value);
                    Console.WriteLine("Change Rate Value (1 - 5), Zero(0) for no chnage :");
                    string x = veg1.Value;

                    string rate = Console.ReadLine();
                    if (rate != "0")
                    {
                        vegList[x] = rate;
                    }
                }
            }
        }
    }
}
